import {getSinglePageTemplate} from './templates/get-single-page-template'
import {fetchCharacterData} from './fetch-character-data';

export const displayCharacterData = rootElement => {
    const loadCharacter = id => {
        fetchCharacterData(id).then(response => {
            const html = response.data.results.map(({id, name, description, thumbnail}) => (
                getSinglePageTemplate({
                    id,
                    name,
                    description,
                    thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
                })
            )).join('');

            rootElement.insertAdjacentHTML('beforeend', html);
        });
    };

    return {loadCharacter};
};
